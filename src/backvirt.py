#!/usr/bin/env python3
import os
import subprocess
from datetime import datetime

import libvirt
from lxml import objectify


# Trying to make a decent script to backup some vms
class BackVirt():
    days_to_keep = 7
    borg_repo_dir = '/mnt/backups/backvirt'
    complete_copies_storage = '/var/backups/libvirt'
    images_pool = ('/var/lib/libvirt/images')  # List of directories where images disk lives
    snapshot_name = 'snapshot.qcow2'

    def __init__(self):
        self.check_some_things()
        self.conn = libvirt.open('qemu:///system')
        self.__get_domain_specs()
        self.__first_of_month()

    def check_some_things(self):
        # Check for some stuff, this only checks for the existence of repo dir
        # it's your duty to initialize with borg init
        try:
            os.stat(self.borg_repo_dir)
        except FileNotFoundError:
            print('Borg repo does not exists')
            exit(-1)

    def do_the_stuff(self):
        for vm in self.domain_specs.items():
            self.__make_snapshot(vm)
        self.__prune_archive()

    def __get_domain_specs(self):
        IDs = self.conn.listDomainsID()
        self.domain_specs = {}
        for id in IDs:
            items = []
            domain = self.conn.lookupByID(id)
            name = domain.name()
            xml_raw = domain.XMLDesc()
            tree = objectify.fromstring(xml_raw)

            for e in tree.devices.disk:
                target = e.target.attrib
                target = target['dev']
                # CDROM devices don't have a source
                try:
                    source = e.source.attrib
                    source = source['file']
                except AttributeError:
                    break
                items.append((target, source))
            self.domain_specs[name] = items

    def __get_current_date(self):
        d = datetime.now()
        self.current_date = d.strftime('%d-%m-%Y-%H:%M')

    def __make_snapshot(self, vm):

        self.__get_current_date()
        self.name = vm[0]
        disk, target = vm[1][0]
        # Conect to default uri qemu
        arg = '{0} {1}'.format('connect', 'qemu:///system')
        try:
            proc = subprocess.check_output(['virsh', arg])
        except subprocess.SubprocessError as err:
            print(err)
            exit(-1)

        # Stop writes to the current VM and create snapshot file.
        print('Backing up {0}'.format(self.name))
        print('\n')
        arg1 = '{0},snapshot=external'.format(disk)
        try:
            proc = subprocess.check_output(
                ['virsh', 'snapshot-create-as', '--domain', self.name, self.snapshot_name, '--no-metadata',
                 '--diskspec', arg1, '--disk-only', '--atomic']
            )
        except subprocess.SubprocessError as err:
            print(err)
            exit(-1)

        for v in vm[1]:
            # Make a full copy of disk if first day of month
            disk, target = v
            if self.__first_of_month():
                pass
                # TODO: Make copies of images

            # borg backups
            self.__make_archive(disk, target)

            # Roll the snapshot changes back into the base file.
            try:
                proc = subprocess.check_output(['virsh', 'blockcommit', self.name, disk, '--active', '--pivot'])
            except subprocess.SubprocessError as err:
                print(target, err)
                exit(-1)

            self.__remove_snapshot(target)

    def __first_of_month(self):
        today = datetime.today()
        if today.day is 1:
            self.first_of_month = True
        else:
            self.first_of_month = False

    def __remove_snapshot(self, target):
        snapshot = os.path.basename(target)
        snapshot = os.path.splitext(snapshot)
        snapshot = snapshot[0]
        snapshot = '{0}.{1}'.format(snapshot, self.snapshot_name)
        # snapshot = os.path.join(self.temp_dir, snapshot)
        for d in self.images_pool:
            to_delete = os.path.join(d, snapshot)
            if os.path.exists(to_delete):
                try:
                    os.unlink(to_delete)
                except OSError as e:
                    print('Can not delete {0},{1}'.format(to_delete, e))
                    return

    def __make_archive(self, disk, target):
        # Borgbackup don't have a public API https://github.com/borgbackup/borg/issues/654
        # so, we need to use shell commands
        arg = "{0}::{1}-{2}_{3}".format(self.borg_repo_dir, self.name, disk, self.current_date)
        try:
            proc = subprocess.check_output(['borg', 'create', arg, target])
        except subprocess.SubprocessError as err:
            print(target, err)
            exit(-1)

    def __prune_archive(self):
        # do the prune
        arg = "{0}d".format(self.days_to_keep)
        try:
            subprocess.check_output(['borg', 'prune', '--keep-within', arg, self.borg_repo_dir])
        except subprocess.SubprocessError as err:
            print(err)
            exit(-1)


if __name__ == '__main__':
    bv = BackVirt()
    if bv.domain_specs:
        bv.do_the_stuff()
