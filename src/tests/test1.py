import libvirt
from lxml import objectify

if __name__=='__main__':
    conn = libvirt.open('qemu:///system')
    IDs = conn.listDomainsID()
    domain_specs = {}
    for id in IDs:
        list = []
        domain = conn.lookupByID(id)
        name = domain.name()
        xml_raw = domain.XMLDesc()
        tree = objectify.fromstring(xml_raw)

        for e in tree.devices.disk:
            target = e.target.attrib
            target = target['dev']
            source = e.source.attrib
            source = source['file']
            list.append((target,source))
        domain_specs[name] = list

    for i in domain_specs.items():
        print (i)
